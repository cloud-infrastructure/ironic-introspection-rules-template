#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""This script will take json file with 1 rule with multiple conditions
and split it to multiple json files with 1 rule with 1 condition.
"""

import argparse
import os
import sys
import json
import copy
from pprint import pprint
parser = argparse.ArgumentParser(description='Script description')

# Optional Arguments
parser.add_argument("-i", "--input_rule",
                    help="Specify input file",
                    type=argparse.FileType('r'),
                    required=True)
parser.add_argument("-o", "--output_rule",
                    help="Specify output dir",
                    required=True)

args = parser.parse_args()

def check_json_list_field(json_, field):
    t = json_.get(field)
    assert t, "'{}' field is empty".format(field)
    assert type(t) is list, "'{}' field is not a list".format(field) 
    assert len(t) > 0, "'{}' field is empty list".format(field)

def main():
    rule_dict = None
    rules_list = []  # list of result rules
    try:
        rule_dict = json.load(args.input_rule)
        check_json_list_field(rule_dict, 'conditions')
        check_json_list_field(rule_dict, 'actions')
        assert 'description' in rule_dict, "Rule does not contain 'description' field"
    except Exception as e:
        print("An error occurred while parsing input file :", str(e))
        sys.exit(1)

    for c in rule_dict.get('conditions'):
        r = copy.deepcopy(rule_dict)
        r['description'] = "{description} - check if '{field}' '{op}' '{value}'".format(
            description=rule_dict['description'],
            **c
        )
        r['conditions'] = [c]
        for a in r.get('actions'):
            a['message'] = "{} : '{field}' '{op}' '{value}'".format(a.get('message'), **c)
        rules_list.append(r)
    with open(args.output_rule, 'w') as out:
        json.dump(rules_list, out, indent=4)

if __name__ == '__main__':
    main()