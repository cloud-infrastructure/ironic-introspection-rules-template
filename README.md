# ironic-introspection-rules-template

This repo contains a script to generate ironic inspection rules as well as some examples.

## Why we need a generator script

At the moment, ironic inspection rules have limitation that conditions are matched using AND statement, not.
Meaning that all the fields in the rule need to happen for the action to be applied. This means that if in the rule
we are checking that CPU and RAM has to be a specific size, and only RAM size is not the size we have expected, the
action will not be taken. We need both fields to be fail the check.

The workaround is to simple create 1 rule per condition but it can get tedious to maintain it. This script solves it by
splitting 1 rule with multiple conditions into multiple rules with 1 condition.

`example_rules/delivery_cd6428773.json` was created based on inspected inspection data from command bellow:

```bash
openstack baremetal introspection data save cd6428773-490-1 | jq .
```

## Using the script

Create `rule.json` file using `example_rules/cd6428773_rule.json` as a template. Then generate new rules executing:

```bash
./rule_generator.py -i <input_rule.json> -o <new_rules.json>
```

Then load import the rules:

```bash
openstack baremetal introspection rule import rule_custom.json
```

Check if rules were imported

```bash
openstack baremetal introspection rule list --fit
```

Drop already loaded rules

```bash
openstack baremetal introspection rule purge
```

## Example json of inspect node's data

This example was creating by inspecting devstack test node running the command bellow:

```bash
openstack baremetal introspection data save node-0 | jq .
```

```json
{
  "inventory": {
    "interfaces": [
      {
        "name": "ens1",
        "mac_address": "52:54:00:9a:f0:34",
        "ipv4_address": "172.24.42.100",
        "ipv6_address": "fd3d:9cba:df0:0:5054:ff:fe9a:f034",
        "has_carrier": true,
        "lldp": null,
        "vendor": "0x8086",
        "product": "0x100e",
        "client_id": null,
        "biosdevname": null
      },
      {
        "name": "ens2",
        "mac_address": "52:54:00:a9:ad:1d",
        "ipv4_address": "172.24.42.101",
        "ipv6_address": "fd3d:9cba:df0:0:5054:ff:fea9:ad1d",
        "has_carrier": true,
        "lldp": null,
        "vendor": "0x8086",
        "product": "0x100e",
        "client_id": null,
        "biosdevname": null
      }
    ],
    "cpu": {
      "model_name": "QEMU Virtual CPU version 2.5+",
      "frequency": "2394.287",
      "count": 1,
      "architecture": "x86_64",
      "flags": [
        "fpu",
        "de",
        "pse",
        "tsc",
        "msr",
        "pae",
        "mce",
        "cx8",
        "apic",
        "sep",
        "mtrr",
        "pge",
        "mca",
        "cmov",
        "pat",
        "pse36",
        "clflush",
        "mmx",
        "fxsr",
        "sse",
        "sse2",
        "syscall",
        "nx",
        "lm",
        "nopl",
        "cpuid",
        "pni",
        "cx16",
        "hypervisor",
        "lahf_lm",
        "svm",
        "3dnowprefetch",
        "vmmcall"
      ]
    },
    "disks": [
      {
        "name": "/dev/vda",
        "model": "",
        "size": 11811160064,
        "rotational": true,
        "wwn": null,
        "serial": null,
        "vendor": "0x1af4",
        "wwn_with_extension": null,
        "wwn_vendor_extension": null,
        "hctl": null,
        "by_path": "/dev/disk/by-path/pci-0000:00:0a.0"
      }
    ],
    "memory": {
      "total": 2086195200,
      "physical_mb": 2048
    },
    "bmc_address": "0.0.0.0",
    "bmc_v6address": "::/0",
    "system_vendor": {
      "product_name": "Standard PC (i440FX + PIIX, 1996)",
      "serial_number": "",
      "manufacturer": "QEMU"
    },
    "boot": {
      "current_boot_mode": "bios",
      "pxe_interface": "52:54:00:9a:f0:34"
    },
    "hostname": "localhost"
  },
  "root_disk": {
    "name": "/dev/vda",
    "model": "",
    "size": 11811160064,
    "rotational": true,
    "wwn": null,
    "serial": null,
    "vendor": "0x1af4",
    "wwn_with_extension": null,
    "wwn_vendor_extension": null,
    "hctl": null,
    "by_path": "/dev/disk/by-path/pci-0000:00:0a.0"
  },
  "boot_interface": "52:54:00:9a:f0:34",
  "configuration": {
    "collectors": [
      "default",
      "logs",
      "pci-devices"
    ],
    "managers": [
      {
        "name": "generic_hardware_manager",
        "version": "1.1"
      }
    ]
  },
  "pci_devices": [
    {
      "vendor_id": "8086",
      "product_id": "100e"
    },
    {
      "vendor_id": "8086",
      "product_id": "7020"
    },
    {
      "vendor_id": "8086",
      "product_id": "7000"
    },
    {
      "vendor_id": "1af4",
      "product_id": "1002"
    },
    {
      "vendor_id": "8086",
      "product_id": "1237"
    },
    {
      "vendor_id": "8086",
      "product_id": "7113"
    },
    {
      "vendor_id": "8086",
      "product_id": "7010"
    },
    {
      "vendor_id": "8086",
      "product_id": "100e"
    },
    {
      "vendor_id": "1b36",
      "product_id": "0001"
    },
    {
      "vendor_id": "1af4",
      "product_id": "1001"
    }
  ],
  "error": null,
  "ipmi_address": null,
  "ipmi_v6address": null,
  "all_interfaces": {
    "ens1": {
      "ip": "172.24.42.100",
      "mac": "52:54:00:9a:f0:34",
      "client_id": null,
      "pxe": true
    },
    "ens2": {
      "ip": "172.24.42.101",
      "mac": "52:54:00:a9:ad:1d",
      "client_id": null,
      "pxe": false
    }
  },
  "interfaces": {
    "ens1": {
      "ip": "172.24.42.100",
      "mac": "52:54:00:9a:f0:34",
      "client_id": null,
      "pxe": true
    }
  },
  "macs": [
    "52:54:00:9a:f0:34"
  ],
  "local_gb": 10,
  "cpus": 1,
  "cpu_arch": "x86_64",
  "memory_mb": 2048
}
```
